package com.citi.demohandson;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import com.citi.demohandson.entities.Trade;
import com.citi.demohandson.services.TradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ServiceTests {
    @Autowired
    TradeService tradeService;
    @Test
    public void testFindAllReturns(){
        List<Trade> trades = tradeService.findall();
        int actual = trades.size();
        assertTrue(actual>0);



    }

    
}