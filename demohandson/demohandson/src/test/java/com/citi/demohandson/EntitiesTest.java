package com.citi.demohandson;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.citi.demohandson.entities.Trade;
import com.citi.demohandson.entities.TradeState;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EntitiesTest {
    @Test()
    public void TestVolumeGeneratesATestException(){
        Exception exception = Assertions.assertThrows(TradeException.class,
 ()->{
    Trade trade = new Trade();
    trade.setquantity(-9);}
  );

    }

    @Test

    public void testConstructor(){
      String id = "abcd";
      Date cdate = new Date();
      double quantity = 45.34;
      String tradetype = "BUY";
      TradeState tradeState = TradeState.FILLED;
      Date tradeDate = new Date();
      String ticker = "AAPL";
      double amount = 33.3;

      Trade trade = new Trade(id, cdate, quantity, tradetype, tradeState, tradeDate, ticker, amount);

      assertEquals(trade.getId(), id);
      assertEquals(trade.getCdate(), cdate);
      assertEquals(trade.getquantity(), quantity);
      assertEquals(trade.getTradetype(), tradetype);
      assertEquals(trade.getstate(), tradeState);
      assertEquals(trade.getTradedate(), tradeDate);
      assertEquals(trade.getticker(), ticker);
      assertEquals(trade.getAmount(), amount);

    }
    
}