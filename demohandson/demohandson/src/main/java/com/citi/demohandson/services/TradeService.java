package com.citi.demohandson.services;

import java.util.List;

import com.citi.demohandson.dao.TradedaoMongo;
//import com.citi.demohandson.dao.TradedaoMySql;
import com.citi.demohandson.entities.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeService implements ITradeService {
    @Autowired
    TradedaoMongo dao;

    @Override
    public List<Trade> findall() {
        // TODO Auto-generated method stub
        return dao.findAll();
    }

    @Override
    public Trade findbyId(String id) {
        // TODO Auto-generated method stub
        return dao.findById(id);
    }

    @Override
    public Trade add(Trade trade) {
        // TODO Auto-generated method stub
        return dao.place(trade);
    }

    @Override
    public void delete(String id) {
        dao.delete(id);
        // TODO Auto-generated method stub

    }
    @Override
    public Trade update(Trade trade){
        //return dao.updateState(trade.getId());
        return dao.updateState(trade);
    }
    @Override
    public void update(String id, int quantity){
        //return dao.updateState(trade.getId());
         dao.updateState(id,quantity);
    }
    
}