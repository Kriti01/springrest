package com.citi.demohandson.rest;

import java.util.*;

import java.util.List;

import com.citi.demohandson.ResourceNotFoundException;
//import com.citi.demohandson.dao.Tradedao;
//import com.citi.demohandson.dao.TradedaoMongo;
//import com.citi.demohandson.TradeException;
import com.citi.demohandson.entities.Trade;
import com.citi.demohandson.services.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/trades")
public class TradeController {
    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
    @Autowired
    private TradeService tradeService;
    
    
    @RequestMapping(value = "getStatus",method = RequestMethod.GET)
    public String getStatus(){
        return "Server is running";
        
    }

    @RequestMapping(value = "getall", method = RequestMethod.GET)
    public List<Trade> getall(){
        LOG.debug("getall() called...........");
        return tradeService.findall();
    }

    @RequestMapping(value = "getbyid/{id}", method = RequestMethod.GET)
    public Trade findById(@PathVariable String id) throws ResourceNotFoundException{
        Trade trade = tradeService.findbyId(id);

        if(trade == null){
            throw new ResourceNotFoundException("Id not found :: " + id);
        }

       
        return trade;
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    Trade trade(@RequestBody Trade trade) throws ResourceNotFoundException {

        Trade trad = tradeService.findbyId(trade.getId());
        
        if(trad != null){
            throw new ResourceNotFoundException("Id not found :: " + trade.getId());
        }


    return tradeService.add(trade);
  }

  @RequestMapping(value = "deletebyid/{id}",method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) throws ResourceNotFoundException {
        Trade trade = tradeService.findbyId(id);

        if(trade == null){
            throw new ResourceNotFoundException("Id not found :: " + id);
        }

        tradeService.delete(id);

    }
    
    @PutMapping("updatebyid/{id}")
    public Trade update(@PathVariable String id) throws ResourceNotFoundException{
        //Trade trade = tradeService.findbyId(id).orElseThrow(() -> new ResourceNotFoundException("Id not found :: " + id));
        
        Trade trade = tradeService.findbyId(id);
        System.out.println(trade);

        if(trade == null){
            throw new ResourceNotFoundException("Id not found :: " + id);
        }
        
        tradeService.update(trade);
        return trade;
    }

    @PutMapping("updateby/{id}/{quantity}")
    public void update( @PathVariable String id, @PathVariable int quantity) throws ResourceNotFoundException {
        Trade trade = tradeService.findbyId(id);

        if(trade == null){
            throw new ResourceNotFoundException("Id not found :: " + id);
        }

        tradeService.update(id,quantity);
    }


    

    




    
}