package com.citi.demohandson.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Entity {
    @Id
    private String id;
    private Date cdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public Entity(String id, Date cdate) {
        this.id = id;
        this.cdate = cdate;
    }
    public Entity(){
        
    }

    @Override
    public String toString() {
        return "Entity [cdate=" + cdate + ", id=" + id + "]";
    }
    
}