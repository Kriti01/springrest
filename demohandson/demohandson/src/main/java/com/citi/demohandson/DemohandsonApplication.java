package com.citi.demohandson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemohandsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemohandsonApplication.class, args);
	}

}
