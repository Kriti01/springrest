package com.citi.demohandson.dao;

import java.util.List;

import com.citi.demohandson.entities.Trade;

public interface Tradedao {
    Long rowCount();
    List<Trade> findAll();
    Trade findById(String id);
    Trade place(Trade trade);
    Trade updateState(Trade trade);
    int clear();
    void delete(String id);
    void updateState(String id, int volume);


    
}