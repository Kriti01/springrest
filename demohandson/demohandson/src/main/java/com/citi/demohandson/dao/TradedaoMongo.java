package com.citi.demohandson.dao;

import java.util.List;

import com.citi.demohandson.entities.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class TradedaoMongo implements Tradedao {
    @Autowired
    private MongoTemplate tpl;

    @Override
    public Long rowCount() {
        // TODO Auto-generated method stub
        Query query = new Query();
       Long result= tpl.count(query, Trade.class);
       return result;

    }

    @Override
    public List<Trade> findAll() {
        // TODO Auto-generated method stub
        return tpl.findAll(Trade.class);
    }

    @Override
    public Trade findById(String id) {
        // TODO Auto-generated method stub
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Trade trade = tpl.findOne(query, Trade.class);
        return trade;
    }

    @Override
    public Trade place(Trade trade) {
        // TODO Auto-generated method stub
       tpl.insert(trade);
       return null;
    }


    @Override
    public int clear() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void delete(String id) {
        // TODO Auto-generated method stub
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        tpl.remove(query, Trade.class);

    }

    @Override
    public Trade updateState(Trade trade) {
        // TODO Auto-generated method stub
        Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("id").is(trade.getId()));
        update.set("cdate",trade.getCdate());
        update.set("quantity",trade.getquantity());
        update.set("tradetype",trade.getTradetype());
        update.set("status",trade.getstate());
        update.set("amount",trade.getAmount());
        update.set("ticker",trade.getticker());
        return tpl.findAndModify(query, update, Trade.class);
    }

    @Override
    public void updateState(String id, int quantity) {
        // TODO Auto-generated method stub
        Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("id").is(id));
        Trade trade = tpl.findOne(query, Trade.class);
        update.set("quantity",quantity);
        update.set("cdate",trade.getCdate());
        update.set("tradetype",trade.getTradetype());
        update.set("status",trade.getstate());
        update.set("amount",trade.getAmount());
        update.set("ticker",trade.getticker());
        tpl.findAndModify(query, update, Trade.class);
        


    }



    
}