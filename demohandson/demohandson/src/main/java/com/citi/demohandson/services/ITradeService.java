package com.citi.demohandson.services;

import java.util.List;

import com.citi.demohandson.entities.Trade;

public interface ITradeService {
    List<Trade> findall();
    Trade findbyId(String id);
    Trade add(Trade trade);
    void delete(String id);
    Trade update(Trade trade);
    void update(String id, int quantity);
    
}